import org.apache.tools.ant.*;

import java.io.File;
import java.util.Vector;

/**
 * Created by Romanyuk Dmitriy on 08.07.2017.
 */
public class BuildFileValidator extends Task {

    private boolean checkDepends = false;
    private boolean checkDefaults = false;
    private boolean checkNames = false;
    private Vector<BuildFile> buildFiles = new Vector<>();

    /**
     * @param checkDepends - parameter to indicate whether to check depends.
     */
    public void setCheckDepends(final boolean checkDepends) {
        this.checkDepends = checkDepends;
    }

    /**
     * @param checkDefaults - parameter to indicate whether to do check defaults.
     */
    public void setCheckDefaults(final boolean checkDefaults) {
        this.checkDefaults = checkDefaults;
    }

    /**
     * @param checkNames - parameter to indicate whether to check names.
     */
    public void setCheckNames(final boolean checkNames) {
        this.checkNames = checkNames;
    }

    /**
     * Do the work.
     */
    public void execute() {

        for (BuildFile file : buildFiles) {
            if (checkDepends) {
                CheckBuildFile.checkBuildFile(Checker.CHECK_DEPENDS, file.getFile());
            }
            if (checkDefaults) {
                CheckBuildFile.checkBuildFile(Checker.CHECK_DEFAULTS, file.getFile());
            }
            if (checkNames) {
                CheckBuildFile.checkBuildFile(Checker.CHECK_NAMES, file.getFile());
            }
        }
        log("All files is good");
    }

    /**
     * Factory method for creating nested 'buildFile's.
     *
     * @return object of BuildFile.
     */
    public BuildFile createBuildFile() {
        BuildFile buildFile = new BuildFile();
        buildFiles.add(buildFile);
        return buildFile;
    }

    /**
     * A nested 'BuildFile'.
     */
    public class BuildFile {

        private String location;
        private File file;
        /**
         * Constructor of nested class BuildFile.
         */
        public BuildFile() {
        }

        /**
         * @return location that shows where the build-file is located.
         */
        public String getLocation() {
            return location;
        }

        /**
         * @param location - that shows where the build-file is located.
         */
        public void setLocation(final String location) {
            this.location = location;
            file = new File(location);
        }

        /**
         * @return build-file.
         */
        public File getFile() {
            return file;
        }

        /**
         * @param file - build-file.
         */
        public void setFile(final File file) {
            this.file = file;
            location = file.getAbsolutePath();
        }
    }
}
