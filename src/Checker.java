/**
 * Created by Romanyuk Dmitriy on 09.07.2017.
 */
public enum Checker {
    /**
     * a pointer to indicate whether to check depends in build-file.
     */
    CHECK_DEPENDS,
    /**
     * a pointer to indicate whether to check defaults in build-file.
     */
    CHECK_DEFAULTS,
    /**
     * a pointer to indicate whether to check names in build-file.
     */
    CHECK_NAMES
}
