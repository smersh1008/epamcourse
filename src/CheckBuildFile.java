import java.io.File;

/**
 * Created by Romanyuk Dmitriy on 09.07.2017.
 */
public final class CheckBuildFile {

    /**
     * Constructor of class CheckBuildFile.
     */
    private CheckBuildFile() {
    }

    /**
     * This method check buildFile with pointer checker.
     *
     * @param checker - a pointer to what the test should do.
     * @param buildFile - the file you want to test.
     */
    public static void checkBuildFile(final Checker checker, final File buildFile) {
        CheckInBuildFile checkInBuildFile;
        switch (checker) {
            case CHECK_DEPENDS:
                checkInBuildFile = new CheckDependsInBuildFile();
                break;
            case CHECK_DEFAULTS:
                checkInBuildFile = new CheckDefaultsInBuildFile();
                break;
            case CHECK_NAMES:
                checkInBuildFile = new CheckNamesInBuildFile();
                break;
            default:
                throw new NullPointerException();
        }
        checkInBuildFile.checkBuildFile(buildFile);
    }
}
