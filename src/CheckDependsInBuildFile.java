import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.Target;

import java.io.File;
import java.util.Enumeration;
import java.util.Hashtable;

/**
 * Created by Romanyuk Dmitriy on 09.07.2017.
 */
public class CheckDependsInBuildFile extends CheckInBuildFile {

    private static final String MAIN_TARGET = "main";

    @Override
    public void checkBuildFile(final File buildFile) {
        Project project = createProjectFromBuildFile(buildFile);
        Hashtable<String, Target> targets = project.getTargets();
        Enumeration<String> keys = targets.keys();

        while (keys.hasMoreElements()) {
            String key = keys.nextElement();
            Target value = targets.get(key);
            if (!value.getName().equals(MAIN_TARGET)) {
                Enumeration<String> depends = value.getDependencies();
                if (depends.hasMoreElements()) {
                    throw new BuildException(String.valueOf("project " + project.getName()
                            + " has depends in target " + value.getName()));
                }
            }
        }
    }
}
