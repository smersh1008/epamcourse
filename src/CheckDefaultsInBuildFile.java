import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Project;

import java.io.File;

/**
 * Created by Romanyuk Dmitriy on 09.07.2017.
 */
public class CheckDefaultsInBuildFile extends CheckInBuildFile {

    @Override
    public void checkBuildFile(final File buildFile) {
        Project project = createProjectFromBuildFile(buildFile);
        final String projectName = project.getName();
        final String projectDefault = project.getDefaultTarget();
        File projectBaseDir = project.getBaseDir();
        if (projectName == null || projectDefault == null || projectBaseDir == null) {
            throw new BuildException(String.valueOf("project " + project.getName()
                    + " hasn't name, default or valid basedir"));
        }
    }
}
