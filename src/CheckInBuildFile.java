import org.apache.tools.ant.Project;
import org.apache.tools.ant.ProjectHelper;

import java.io.File;

/**
 * Created by Romanyuk Dmitriy on 09.07.2017.
 */
public abstract class CheckInBuildFile {
    Project createProjectFromPathToBuildFile(final String pathToBuildFile) {
        File buildFile = new File(pathToBuildFile);
        return createProjectFromBuildFile(buildFile);
    }

    Project createProjectFromBuildFile(final File buildFile) {
        ProjectHelper helper = ProjectHelper.getProjectHelper();
        Project project = new Project();
        helper.parse(project, buildFile);
        return project;
    }

    /**
     * @param buildFile - file that need check.
     */
    public abstract void checkBuildFile(File buildFile);

    /**
     * @param pathToBuildFile - path to file that need check.
     */
    public void checkBuildFile(final String pathToBuildFile) {
        checkBuildFile(new File(pathToBuildFile));
    }
}
