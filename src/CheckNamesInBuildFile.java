import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.Target;

import java.io.File;
import java.util.Enumeration;
import java.util.Hashtable;

/**
 * Created by Romanyuk Dmitriy on 09.07.2017.
 */
public class CheckNamesInBuildFile extends CheckInBuildFile {

    @Override
    public void checkBuildFile(final File buildFile) {
        Project project = createProjectFromBuildFile(buildFile);
        String name = project.getName();
        if (!name.matches("^[a-zA-Z-]+$")) {
            throw new BuildException(String.valueOf("project name " + project.getName()
                    + " not contains only letters with '-'"));
        }

        Hashtable<String, Target> targets = project.getTargets();
        Enumeration<String> keys = targets.keys();

        while (keys.hasMoreElements()) {
            String key = keys.nextElement();
            Target value = targets.get(key);
            if (!value.getName().matches("^[a-zA-Z-]+$") && (!key.equals("") && !value.getName().equals(""))) {
                throw new BuildException(String.valueOf("in project " + project.getName()
                        + " target name " + value.getName() + " not contains only letters with '-'"));
            }
        }
    }
}
